echo on

pushd "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build"
call vcvarsall.bat %ARCH% -vcvars_ver=%VCVARS_VER%
popd

md src
curl -L https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-%OMNI_VER%/omniORB-%OMNI_VER%.tar.bz2/download -o omniORB.tar.bz2 && ^
7z x omniORB.tar.bz2 -so | tar xf - -C src  --strip-components=1 || goto :error

set PYTHONPATH=Python39_%ARCH%
set PATH=C:\%PYTHONPATH%;%PATH%;c:\tools\cygwin\bin

pushd src\src
if not %ARCH% == x86 (
    sed -i "s/-DEBUG/-DEBUG -MACHINE:X64/g" ..\mk\platforms\x86_win32_vs_15.mk || goto :error
)
sed -i "s/^#platform = x86_win32_vs_15/platform = x86_win32_vs_15/g" ..\config\config.mk && ^
sed -i "s-^#PYTHON = /cygdrive/c/Python36/python-PYTHON = /cygdrive/c/%PYTHONPATH%/python-g" ..\mk\platforms\x86_win32_vs_15.mk && ^
make -j%NUMBER_OF_PROCESSORS% export || goto :error
popd

md dist
xcopy src\bin dist\bin /E/Y/I && ^
xcopy src\lib dist\lib /E/Y/I && ^
xcopy src\include dist\include /E/Y/I || goto :error

md deploy
cd dist
7z a ..\deploy\%DIST_NAME% .

goto :EOF

:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
